
FROM openjdk:latest
RUN mkdir -p /opt/app
ENV PROJECT_HOME /opt/app
COPY target/hello-world-0.1.0.jar $PROJECT_HOME/hello-world-0.1.0.jar
WORKDIR $PROJECT_HOME
EXPOSE 8080
CMD ["java" ,"-jar","./hello-world-0.1.0.jar"]
